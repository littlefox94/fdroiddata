Categories:Office
License:MIT
Web Site:https://github.com/premnirmal/StockTicker/blob/HEAD/README.md
Source Code:https://github.com/premnirmal/StockTicker
Issue Tracker:https://github.com/premnirmal/StockTicker/issues

Auto Name:StockTicker
Summary:Stock market ticker widget
Description:
Widget that shows your stock portolio in a resizable grid. You can sort the
list by drag-and-drop and the list can be exported and re-imported.
.

Repo Type:git
Repo:https://github.com/premnirmal/StockTicker

Build:3.20.00,19
    disable=https://github.com/premnirmal/StockTicker/issues/1
    commit=ac40203d3f17da83cb1a22ab29fe1fc87ed4d3c5
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/applicationId/d' build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.20.00
Current Version Code:19

